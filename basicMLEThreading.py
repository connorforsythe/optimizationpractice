from multiprocessing import Pool
import scipy.stats as stat
import scipy.optimize as opt
import numpy as np
from datetime import datetime as dt

def splitList(x, n):
    #x is the original list
    #n is the number of bins wanted

    size = len(x) // n
    end = len(x)
    temp = []

    if(np.mod(len(x),n)==0):
        n=n
    else:
        n=n+1
    i = 0
    r = []
    while(i<n):
        r.append(i)
        i = i+1

    for i in r:
        if((i+1)+size>len(x)):
            temp.append(x[i*size:end])
        else:
            temp.append(x[i*size:(i+1)*size])
    return temp

def likelihood(mean, sample):
    s = 0
    for el in sample:
        s = s+stat.expon.logpdf(el, scale=mean)
    return s

def negativeLL(mean, sample):
    return -likelihood(mean,sample)

def threadNegLL(mean, sample, n):
    newSamp = splitList(sample,n)
    temp = []

    for samp in newSamp:
        temp.append((mean,samp))
    with Pool() as pool:
        res = pool.starmap(negativeLL,temp)

    return np.sum(res)

def threadNegLL2(mean, sample):
    temp = []
    for samp in sample:
        temp.append((mean,samp))
    with Pool() as pool:
        res = pool.starmap(negativeLL,temp)

    return np.sum(res)


sampleSize = 5000
threadCount = 8
mean = 5
b = [(0.000001,10)]

sample = stat.expon.rvs(scale=mean, size=sampleSize).tolist()


range = slice(0.000001,10,1)


start = dt.now()
res = opt.shgo(negativeLL, bounds=b, args = (sample,))
finish = dt.now()
print("SHGO Res: {}; SHGO Time: {}".format(res.x, finish-start))


start = dt.now()
res = opt.brute(negativeLL, (range,), args = (sample,), finish = opt.fmin)
finish = dt.now()

print("Brute Force Res: {}; Brute Force Time: {}".format(res, finish-start))

start = dt.now()
res = opt.shgo(threadNegLL, bounds=b, args = (sample,threadCount))
finish = dt.now()
print("SHGO (Parralel) Res: {}; SHGO (Parralel) Time: {}".format(res.x, finish-start))

start = dt.now()
res = opt.brute(threadNegLL, (range,), args = (sample, threadCount), finish=opt.fmin)
finish = dt.now()

print("Brute Force (Parralel) Res: {}; Brute Force (Parralel) Time: {}".format(res, finish-start))

