import matplotlib.pyplot as plot
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import scipy.stats as stat
import numpy as np

mean = [0,0]
cvm = [[1,0],[0,1]]
n = 1000
samp = stat.multivariate_normal.rvs(mean=mean, cov=cvm, size=n)

print(samp)
x = []
y = []
z1 = []
z2 = []
for el in samp:
    x.append(el[0])
    y.append(el[1])
    z1.append(stat.multivariate_normal.pdf(x=el,mean=mean,cov=cvm))
    z2.append(stat.multivariate_normal.cdf(x=el, mean=mean, cov=cvm))



# X = np.arange(-5, 5, 0.25)
# Y = np.arange(-5, 5, 0.25)
# X, Y = np.meshgrid(X, Y)
# R = np.sqrt(X**2 + Y**2)
# Z = np.sin(R)

# print(Z)
# print(Z.ndim)
fig = plot.figure()
ax = fig.add_subplot(111, projection='3d')

surf = ax.plot_trisurf(x, y, z2, cmap=cm.coolwarm, linewidth=0, antialiased=False)

plot.show()