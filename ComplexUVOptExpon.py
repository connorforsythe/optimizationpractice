from multiprocessing import Pool
import scipy.stats as stat
import scipy.optimize as opt
import numpy as np
import random as rand
from datetime import datetime as dt
import matplotlib.pyplot as plt

def splitList(x, n):
    #x is the original list
    #n is the number of bins wanted

    size = len(x) // n
    end = len(x)
    temp = []

    if(np.mod(len(x),n)==0):
        n=n
    else:
        n=n+1
    i = 0
    r = []
    while(i<n):
        r.append(i)
        i = i+1

    for i in r:
        if((i+1)+size>len(x)):
            temp.append(x[i*size:end])
        else:
            temp.append(x[i*size:(i+1)*size])
    return temp

#Returns the sum of all threaded likelihood computations
def negLLThreaded(param, samp, n):
    newSamp = splitList(samp,n)

    temp = []
    for s in newSamp:
        temp.append((param,s))

    with Pool() as pool:
        res = pool.starmap(setNegLL, temp)

    return np.sum(res)


def setNegLL(param, samp):
    sum = 0;

    for el in samp:
        tempLL = singNegLL(param, el)

        sum = sum+tempLL
    return sum

#returns the likelihood value for a single value pair
def singNegLL(param, el):
    b0 = param[0]
    b1 = param[1]
    b2 = param[2]
    b3 = param[3]

    tempC = el[0]
    tempX = el[1]

    l = b0+b1*tempC
    s = b2+b3*tempC


    tempDist = stat.norm(loc=l, scale = s)

    ll = tempDist.logpdf(tempX)

    return -ll



def mu(b0, b1, x):
    lol = 0
    m = b0+b1*x

    return m

n = 1000;
c = []
x = []
s = []
sample = []
sample1 = []
b0 = 20
b1 = 0
b2 = 2
b3 = 0
cDist = stat.norm(loc = 100, scale = 10)
threadN = 8



for i in range(0,n):

    tempC = cDist.rvs(size = 1)#rand.randint(0,2)
    tempC = tempC[0]

    #tempC = rand.randint(0,40)
    c.append(tempC)
    loc = b0 + b1 * tempC
    scale = b2+b3*tempC
    s.append(loc)
    xDist = stat.norm(loc = loc, scale = scale)
    tempX = xDist.rvs(size = 1)
    tempX = tempX[0]
    x.append(tempX)
    sample.append([tempC, tempX])
    sample1.append([0,tempX])



param = [b0, b1, b2, b3]



b = [(1e-9,4000),(-10,10), (1e-9,100), (-2+1e-9,10)]
b1 = (b[0], b[1], b[2], b[3])

print("Negative Log Likelihood give the true parameters {}".format(negLLThreaded(param, sample, threadN)))
res = opt.shgo(negLLThreaded, bounds=b, args=(sample, threadN))
res1 = opt.minimize(negLLThreaded, [1,1,1,.5], args=(sample, threadN), method="SLSQP", bounds=b1) #"Nothing to see here."#
# res2 = opt.minimize(negLLThreaded, [1,1,1,.5], args=(sample, threadN), method="L-BFGS-B", bounds=b1)
print("Result SHGO")
print(res)
print("-"*1000)
print(res1)
print("-"*1000)

binNum = 20

plt.hist(x, bins = binNum)
avg = np.average(x)
std = np.std(x)
k2, p = stat.normaltest(x)
alpha = .05
norm = False
m = None

if(p<alpha):
    norm = False
    m = "Non-Normal"
else:
    norm = True
    m = "Normal"

print("The empirical distribution would result in a {} distribution with µ = {} and σ = {}".format(m, avg, std))

# print("Result SLSQP")
# print(res1)
# print("-"*1000)
#
# print("Result L-BFGS-B")
# print(res2)
# print("-"*1000)