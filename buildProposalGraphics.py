import matplotlib.pyplot as plot
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import scipy.stats as stat
import numpy as np

mean = [10,150000]


corr = np.sqrt((35000**2)*25)*.90
cvm = [[25,0],[0,35000**2]]
cvm1 = [[25,-corr],[-corr,35000**2]]
cvm2 = [[25,corr],[corr,35000**2]]

dist = stat.multivariate_normal(mean=mean, cov=cvm)
dist1 = stat.multivariate_normal(mean=mean, cov=cvm1)
dist2 = stat.multivariate_normal(mean=mean, cov=cvm2)


cvmt = np.linspace(0,600000,600)
age = np.linspace(0,40,600)
ga, gc =np.meshgrid(age,cvmt)

vals = np.dstack((ga,gc))

pdf = dist.pdf(vals)
cdf = dist.cdf(vals)
surv = 1-cdf

pdf1 = dist1.pdf(vals)
cdf1 = dist1.cdf(vals)
surv1 = 1-cdf1

pdf2 = dist2.pdf(vals)
cdf2 = dist2.cdf(vals)
surv2 = 1-cdf2

normX = [0, 40]
normY = [0, 40*10000]

lowX = [0, 40]
lowY = [0, 40*5000]

highX = [0, 40]
highY = [0, 40*20000]

#Plot No Correlation
fig = plot.figure().add_subplot(111).contourf(age,cvmt,surv, cmap='PuOr')
cbar = plot.colorbar(fig, label="Survival Probability")
plot.xlabel("Age")
plot.ylabel("CVMT")
plot.title("Survival Space -  Independent")
plot.xlim([0,40])
plot.ylim([0,300000])
plot.plot(highX, highY, '-r', label='High Intensity')
plot.plot(normX, normY, '-y', label='Med. Intensity')
plot.plot(lowX, lowY, '-g', label='Low Intensity')
plot.legend(loc='upper right')

#Plot Negative Correlation
fig1 = plot.figure().add_subplot(111).contourf(age,cvmt,surv1, cmap='PuOr')
cbar1 = plot.colorbar(fig, label="Survival Probability")
plot.xlabel("Age")
plot.ylabel("CVMT")
plot.title("Survival Space -  Negative Corr.")
plot.xlim([0,40])
plot.ylim([0,300000])
plot.plot(highX, highY, '-r', label='High Intensity')
plot.plot(normX, normY, '-y', label='Med. Intensity')
plot.plot(lowX, lowY, '-g', label='Low Intensity')
plot.legend(loc='upper right')

#Plot Postive Correlation
fig2 = plot.figure().add_subplot(111).contourf(age,cvmt,surv2, cmap='PuOr')
cbar2 = plot.colorbar(fig, label="Survival Probability")
plot.xlabel("Age")
plot.ylabel("CVMT")
plot.title("Survival Space -  Positive Corr.")
plot.xlim([0,40])
plot.ylim([0,300000])
plot.plot(highX, highY, '-r', label='High Intensity')
plot.plot(normX, normY, '-y', label='Med. Intensity')
plot.plot(lowX, lowY, '-g', label='Low Intensity')
plot.legend(loc='upper right')

plot.show()

# print("GC")
# print(gc)
#
# print("------------------------------------------------------------")
# print("------------------------------------------------------------")
# print("GA")
# print(ga)


