from multiprocessing.pool import Pool
import numpy as np
from datetime import datetime as dt


def cumSum(start, end):
    s = 0
    for i in range(start, end+1):
        s = s+i
    return s

def splitList(x, n):
    #x is the original list
    #n is the number of bins wanted

    size = len(x) // n
    end = len(x)
    temp = []

    if(np.mod(len(x),n)==0):
        n=n
    else:
        n=n+1

    for i in range(0,n):
        if((i+1)+size>len(x)):
            temp.append(x[i*size:end])
        else:
            temp.append(x[i*size:(i+1)*size])
    return temp

test = [1,2,3,4,5]
print(type(test))

print(test[0:len(test)])

print(splitList(test,2))

def cumSumL(x):
    s = 0
    for i in x:
        s = s+i
    return s

def cumSumP(x):
    mid = x//2
    p = Pool(2)

    t = [(0, mid-1),(mid, x)]

    res = p.starmap(cumSum, [(0,mid-1),(mid,x)])
    print(res)

def product(x1,x2):
    return x1*x2


x1 = 100000000
x2 = 5
x3 = 10
x4 = 20

a1 = []
for i in range(0,x1+1):
    a1.append(i)



with Pool() as pool:
    res = pool.starmap(product, [(x2,x3),(x2,x4)])
    start = dt.now()
    res2 = pool.map(cumSumL, splitList(a1,4))
    print(cumSumL(res2))
    time2 = dt.now()
    res4 = pool.map(cumSumL, splitList(a1,8))
    print(cumSumL(res4))
    time4 = dt.now()
    print(res)

print("Two Split Time: {}".format(time2 - start))
print("Four Split Time: {}".format(time4 - time2))

start = dt.now()
cumSum(0,x1)
finish = dt.now()
cumSumP(x1)
finish2 = dt.now()

print("Non-Parallel Time: {}".format(finish-start))
print("Parallel Time: {}".format(finish2-finish))
