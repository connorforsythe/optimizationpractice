import scipy.stats as stat
import numpy.random as rand
import scipy.optimize as opt
import numpy
from datetime import datetime

def nLikelihood(mean, sample):
    temp = []
    for num in sample:
        temp.append(stat.expon.logpdf(x=num, loc=0, scale=mean))
    s = numpy.sum(temp)
    return -s

def nLikelihood2(mean, sample):
    temp = []
    for num in sample:
        temp.append(stat.expon.logpdf(x=num, loc=0, scale=mean))
    s = numpy.sum(temp)
    return -s

mean = 8
x = 1
p = stat.expon.pdf(x, loc = 0, scale = mean)
m = stat.expon.mean(scale=mean)

n = 10000

samp = stat.expon.rvs(scale=mean,size=n)

# print(nLikelihood(mean,samp))
# print(nLikelihood(mean-1,samp))

x0 = numpy.array([1])
b = [(1,numpy.inf)]


stime1 = datetime.now()
res = opt.shgo(func=nLikelihood, args = (samp,),bounds=b)
stime2 = datetime.now()




range = (slice(0.000001,10,.25))

btime1 = datetime.now()
res2 = opt.brute(nLikelihood, (range,), args=(samp,), full_output=True, finish=opt.fmin)
btime2 = datetime.now()


stime = stime2-stime1

btime = btime2-btime1

print("SHGO")
print(res)
print("Time to complete: {}".format(stime))

print("Brute")
print(res2)
print("Time to complete: {}".format(btime))



