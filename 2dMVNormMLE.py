import scipy.stats as stat
import numpy.random as rand
import scipy.optimize as opt
import numpy
from datetime import datetime

def nLikelihood(param, sample):
    temp = []
    mean = [param[0], param[1]]
    cvm = [[param[2], param[3]],[param[3],param[4]]]

    #print(cvm)

    if((param[2]*param[4]-param[3]**2)<=0):
        return numpy.inf


    for num in sample:
        temp.append(stat.multivariate_normal.logpdf(x=num, mean=mean, cov=cvm))
    s = numpy.sum(temp)
    return -s

def nLikelihood2(param, sample):
    temp = []
    mean = [param[0], param[1]]
    cvm = [[param[2], param[3]],[param[3],param[4]]]
    s = 0

    #print(cvm)

    if((param[2]*param[4]-param[3]**2)<=0):
        return numpy.inf


    for num in sample:
        s = s+stat.multivariate_normal.logpdf(x=num, mean=mean, cov=cvm)
    return -s

def posDefMatrix(param):
    return param[2]*param[4]-param[3]**2-.000001 #≥0

mean = [1.5,1.5]

cvm = [[1,0],[0,1]]

n = 1000000

print("Mean: {}".format(mean))
print("SD: {}".format(cvm))
print("N: {}".format(n))

print("Generating sample")
samp = stat.multivariate_normal.rvs(mean=mean, cov=cvm, size=n)
print("Sample is generated")

par = [0,0,1,0,1]
par2 = [2,2,1,0,1]


b = [(-1,1),(-1,1),(.000001,2),(-2,2),(.000001,2)]

c = [{'type': 'ineq', 'fun':posDefMatrix}]

b2 = ((0,5),(0,5),(0.0000001, 10),(-10,10),(0.000001, 10))

start = datetime.now()
#res = opt.shgo(func=nLikelihood, args = (samp,),bounds=b, constraints=c)
ll1 = nLikelihood(par, samp)
mid = datetime.now()
ll2= nLikelihood2(par,samp)
finish = datetime.now()

print("OG Time: {}".format(mid-start))
print("New Time: {}".format(finish-mid))
#res = opt.minimize(nLikelihood, (2,2,2,-1,2),(samp,),'SLSQP')
#res = opt.brute(nLikelihood, b, args=(samp,), full_output=False, finish=opt.fmin)