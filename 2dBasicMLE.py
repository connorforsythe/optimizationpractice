import scipy.stats as stat
import numpy.random as rand
import scipy.optimize as opt
import numpy
from datetime import datetime

def nLikelihood(param, sample):
    temp = []
    mean = param[0]
    sd = param[1]
    for num in sample:
        temp.append(stat.norm.logpdf(x=num, loc=mean, scale=sd))
    s = numpy.sum(temp)
    return -s

mean = 0
sd = 1
n = 100



print("Mean: {}".format(mean))
print("SD: {}".format(sd))
print("N: {}".format(n))

#samp = rand.normal(loc=mean,scale=sd,size=n)
samp = stat.norm.rvs(loc=mean,scale=sd,size=n)


print(nLikelihood([0,10],samp))
print(nLikelihood([10,10],samp))
print(nLikelihood([0,1],samp))

b = [(-numpy.inf,numpy.inf),(.000001,5e5)]
b2 = [(-numpy.inf,numpy.inf),(.000001,5e10)]

range = (slice(-5,5,.25),slice(0.000001,20,1))

start = datetime.now()
res = opt.shgo(func=nLikelihood, args = (samp,),bounds=b)
tshgo = datetime.now()
res3 = opt.brute(nLikelihood, range, args = (samp,),full_output=False, finish=opt.fmin)
tbrute = datetime.now()
print(res)
print("Time to complete SHGO {}".format(tshgo-start))
print(res3)
print("Time to complete Brute {}".format(tbrute-start))

